function repeatedString(s, n) {
  if (s.length == 1) {
    return n;
  }
  const arr = s.split("");
  const aCount = arr.filter((x) => x == "a").length;
  if(aCount==0){
      return 0;
  }
  if (n <= s.length) {
    return arr.slice(0,n).filter(x=>x=="a").length;
  }
  var ans = aCount * Math.floor(n / s.length);
  var r = n - Math.floor(n / s.length) * s.length;
  if (r >= 1) ans += arr.slice(0, r).filter((x) => x == "a").length;
  return ans;
}
repeatedString("ababa", 3);
