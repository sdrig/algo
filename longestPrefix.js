/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function (strs) {
  let maxPrefix = Infinity;
  let ans = "";
  for (var i = 0; i < strs.length; i++) {   
    maxPrefix = Math.min(maxPrefix, strs[i].length);
  }
  for (var i = 0; i < maxPrefix; i++) {
    if (!strs[0]) {
      return ans;
    }
    var letter = strs[0][i];
    var result = true;
    for (var j = 0; j < strs.length; j++) {
      result &= letter == strs[j][i];
    }
    if (result) {
      ans += letter;
    } else {
      return ans;
    }
  }
  return ans;
};

var arr = [[""], ["flower", "flow", "flight"]];

for (var i = 0; i < arr.length; i++) {
  console.log(longestCommonPrefix(arr[i]));
}
