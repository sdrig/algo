// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');
function solution(S, X, Y) {
  // write your code in JavaScript (Node.js 8.9.4)
  var distanceArray = [];
  var distanceArray2 = [];
  // map distance with letters. to understand two points have the same letter and same distance.
  // I will have  distance => [A,B,C]
  // I wrote the function remove duplicates to understand is there any same letter at the same point.
  // D => A,B,C,A => removeDuplicates => A,B,C
  var map = {};
  var N = X.length;
  for (let i = 0; i < N; i++) {
    var x = X[i];
    var y = Y[i];
    var distance = Math.sqrt(x * x + y * y);
    distanceArray.push(distance);
    if (!distanceArray2[distance]) {
      distanceArray2[distance] = [];
    }
    distanceArray2[distance].push(S[i]);
  }
  distanceArray.sort();
  var ans = 0;
  for (let i = 0; i < N; i++) {
    var distance = distanceArray[i];
    var letters = distanceArray2[distance];
    //  -if (letters.length > 1) removeDuplicates(letters);
    if (new Set(letters).size !== letters.length) {
      return ans;
    }
    for (let j = 0; j < letters.length; j++) {
      if (letters[j + 1] && letters[j + 1] == letter) {
        return ans;
      }
      var letter = letters[j];
      if (!map[letter]) {
        ans++;
      } else {
        return ans;
      }
      map[letter] = true;
    }
  }
  console.log(ans);
}

var points = [
  ["ABCD", [2, -1, 2, -1], [2, -1, 2, -1]],
  ["AA", [2, -1], [2, -1]],
  ["AABBCC", [2, -1, 2, -1, 2, -1], [2, -1, 2, -1, 2, -1]],
  ["AAB", [2, -1, 2], [2, -1, 2]],
];
for (let i = 0; i < points.length; i++) {
  var c = point[i];
  solution(c[0],c[1],c[2]);
}
