// Generate cartesian product of given iterables:
function* cartesian(head, ...tail) {
    let remainder = tail.length ? cartesian(...tail) : [[]];
    for (let r of remainder) for (let h of head) yield [h, ...r];
  }
  
  // Example:
  const first  = ['a', 'b', 'c', 'd'];
//   const second = ['e'];
//   const third  = ['f', 'g', 'h', 'i', 'j'];
  
  console.log(...cartesian(first));