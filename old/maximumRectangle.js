/**
 * @param {character[][]} matrix
 * @return {number}
 */
var maximalRectangle = function(matrix) {
    
    const go = (x,y,grid,start)=>{
        
        go(x+1,y,start);
    }
    go(0,0,matrix);
};

var matrix = [
    ["1","0","1","0","0"],
    ["1","0","1","1","1"],
    ["1","1","1","1","1"],
    ["1","0","0","1","0"]
  ];

  maximalRectangle(matrix);