function solution(A) {
    // write your code in JavaScript (Node.js 8.9.4)
    A = A.filter(x=>x>0);
    
    console.log(A)
    
    if(A.length==0){
        return 1;
    }
    var map = new Map();
    for(let i = 0; i < A.length; i++){
        if(!map.has(A[i])){
            map.set(A[i],A[i]);
        }
    }
    for(let i =1; i<= A.length + 1 ;i++){
        if(i==A.length+1){
            return  A[A.length-1]+1;
        }
        if(!map.has(i)){
            return i;
        }
    }
}

a = [
    [2],
    [-1,-2],
    [-1,1,2,2,3,3,4,4,5,6],
    [1,2],
    [2],
]

for(var arr of a){
    var res = solution(arr);
    console.log(res);
}