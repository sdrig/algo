var productExceptSelf = function(nums) {
    var output = new Array(nums.length).fill(1);
    var leftMult = 1;
    var rightMult = 1;

   
    for (var i=nums.length - 1; i >= 0; i--) {
        output[i] = rightMult;
        rightMult *= nums[i];
    }
    for (var j=0; j < nums.length; j++) {
        output[j] *= leftMult;
        leftMult *= nums[j];
    }
    return output;
};

console.log(productExceptSelf([1,2,3,4]))

// console.log(productExceptSelf([3,5,7,4]))