
/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number}
 */
var findMedianSortedArrays = function(nums1, nums2) {
    
    
    let  m = nums1.length, n= nums2.length;
    
    var total = m + n;
    if(m==0 && n==0){
        return 0;
    }
    var arr =[] //new Array(m+n).fill(0);
   
    
    while(nums1.length || nums2.length) {
        
        if((nums1[0]!= undefined  ? nums1[0] : Infinity)  <= (nums2[0] != undefined ? nums2[0] : Infinity)){
            arr.push(nums1.shift())
        }else {
           arr.push(nums2.shift())
        }
    }
    if(total == 1){
        return arr[0];
    }
    var med = 0;
    
    if( total % 2 == 0){
        med = (arr[total/2] + arr[(total/2)-1]) /2;
    }
    else{
        med = arr[~~(total/2)];
    }
    return med;
};

let nums1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
let nums2 = [0,6];
findMedianSortedArrays(nums1,nums2);