// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(N) {
    // write your code in JavaScript (Node.js 8.9.4)
    var half = ~~(N/2)
    var divisors =[];
    for(let i = 1;i <= half; i++){
        if(N % i == 0){
            divisors.push(i);
        }
    }
    //console.log(divisors);
    let start = ~~(divisors.length/2);
    let A = divisors[start]
    let B = divisors[start+1]
    if(N % A == 0 && N % B==0) {
        return 2 * (A + B);
    } else {
        return 4 * A;
    }
    return 9999;
}

solution(1);