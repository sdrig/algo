const coins = (n) => {
  var result = 0;
  var coin = new Array(n+1).fill(0)
  for (var i = 1; i < n+1; i++) {
    let k = i;
    while (k <= n) {
      coin[k] = (coin[k] + 1) % 2;
      k += i;
    }
    result += coin[i];
  }
  return result;
};
coins(10)
