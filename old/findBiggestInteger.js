const test1 = [
  [9, 1, 1, 0, 7],
  [1, 0, 2, 1, 0],
  [1, 9, 1, 1, 0],
];

var maxInt;
var maxIntPath;
var loc;
function biggestIntInMatrix(grid) {
  maxInt = 0;
  let maxGridVal = -Infinity;
  const stack = [];
  for (var i = 0; i < grid.length; i++) {
    for (var j = 0; j < grid[i].length; j++) {
      if (grid[i][j] > maxGridVal) {
        maxGridVal = grid[i][j];
        stack.length = 0;
        stack.push([i, j]);
      } else if (grid[i][j] === maxGridVal) {
        stack.push([i, j]);
      }
    }
  }
  console.log(`max grid val ${maxGridVal}`);
  console.log(`max grid val ${stack}`);
  while (stack.length) {
    loc = stack.pop();
    dfs(loc[0], loc[1], grid);
  }
}
function dfs(i, j, grid, visited = null, prev = null, count = 0) {
  // console.log(arguments);
  if (count == 4 || (visited !== null && visited.has(`${i},${j}`))) {
    return;
  }
  const key = `${i},${j}`;
  if (visited == null) {
    visited = new Set();
  }
  visited.add(key);
  const currNum = grid[i][j];
  var str = `${prev}${currNum}`;
  let newInt = 0;
  //   if (str.length == 4) {
  newInt = !prev ? currNum : parseInt(str);
  if (newInt > maxInt) {
    maxInt = Math.max(newInt, maxInt);
    maxIntPath = [...visited];
  }
  //   }
  const dir = [
    [0, 1],
    [0, -1],
    [1, 0],
    [-1, 0],
  ];
  for (const d of dir) {
    const x = i + d[0];
    const y = j + d[1];
    //console.log(x, y);
    if (x < 0 || y < 0 || x >= grid.length || y >= grid[0].length) {
      continue;
    }
    //console.log(`loc:${loc} lookup:${x},${y}; ${[...visited].join("|")}`);
    dfs(x, y, grid, visited, newInt.toString(), count + 1);
  }
  visited.delete(key);
  // console.log("delete");
}

biggestIntInMatrix(test1);

console.log(maxInt, [...maxIntPath].join('|'));
