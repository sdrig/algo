const divisors = (n)=>{
    var i = 1;
    var result = 0;
    while( i * i < n){
        if (n % i == 0)
         {result += 2}
        i += 1
    }
    if(i*i ==n){
        result++
    }
    return result;
}
function solution(N) {
    // write your code in JavaScript (Node.js 8.9.4)
    return divisors(N);
}

console.log(`35 divisors ${solution(35)}`)