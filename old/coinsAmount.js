/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var minCount = Infinity;
var dfs = (i, coins, amount, prevAmount, count, arr) => {
  if (prevAmount >= amount) {
    console.log("r");
    return;
  }
  prevAmount = coins[i] + prevAmount;
  count++;
  arr.push(coins[i]);
  if (prevAmount == amount) {
    minCount = Math.min(minCount, count);
    return;
  }

  for (var j = 0; j < coins.length; j++) {
    dfs(j, coins, amount, prevAmount, count, arr);
    console.log("a:" + coins[i]);
  }
  // console.log(`i:${i},arr:${arr},${prevAmount},${count}`);
  console.log("f");
  arr.pop();
  prevAmount -= coins[i];
  count--;
  return minCount;
};
var coinChange = function (coins, amount) {
  coins.sort((a, b) => b - a);
  var result = dfs(0, coins, amount, 0, 0, []);
  console.log(result);
};
const myCoinChange = (coins, amount) => {
  coins.sort((a, b) => b - a);
  let res = Infinity;
  const find = (k, amount, count) => {
    var coin = coins[k];
    if (k == coins.length - 1) {
      if (amount % coin == 0) {
        res = Math.min(res, count + ~~(amount / coin));
      }
    } else {
      for (let i = ~~(amount / coin); i >= 0 && count + i < res; i--) {
        find(k + 1, amount - coin * i, count + i);
      }
    }
  };
  find(0, amount, 0);
  return res != Infinity ? res : -1;
};

// const coinChange = (coins, amount) => {
//   coins.sort((a, b) => b - a);

//   let res = Infinity;

//   const find = (k, amount, count) => {
//     const coin = coins[k];

//     // last smallest coin
//     if (k === coins.length - 1) {
//       if (amount % coin === 0) {
//         res = Math.min(res, count + ~~(amount / coin));
//       }
//     } else {
//       for (let i = ~~(amount / coin); i >= 0 && count + i < res; i--) {
//         // count + i < res is for pruning, avoid unnecessary calculation
//         find(k + 1, amount - coin * i, count + i);
//       }
//     }
//   };

//   find(0, amount, 0);
//   return res === Infinity ? -1 : res;
// }; 

coinChange([2, 1, 3], 3);
var r = myCoinChange([2, 4, 8, 10], 28);
console.log(r);
