/**
 * @param {string} digits
 * @return {string[]}
 */
var letterCombinations = function (digits) {
  var map = {
    "2": "abc",
    "3": "def",
    "4": "ghi",
    "5": "jkl",
    "6": "mno",
    "7": "pqrs",
    "8": "tuv",
    "9": "wxyz",
  };
  let res = [];
  function dfs(i, s) {
    if (i === digits.length) {
      res.push(s);
      return;
    }

    for (let c of map[digits[i]]) {
      dfs(i + 1, s + c);
    }
  }
  dfs(0, "");
};

letterCombinations("234");
