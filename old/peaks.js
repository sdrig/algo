function solution(A) {
  // write your code in JavaScript (Node.js 8.9.4)
  var size = A.length;
  
  let peakCount = 0;
  for (let i = 1; i < size; i++) {
    if (A[i - 1] < A[i] && A[i] > A[i + 1]) {
      A[i] = "p";
      peakCount++;
    }
  }
  if (peakCount == 1) {
    return 1;
  }
  if (peakCount == 0 || size < 3) {
    return 0;
  }
  let found = false;
  let counter = 1;
  const dfs = (k, index, prev) => {
    if (prev.length == k && index == size) {
      found = true;
      return;
    }
    if (found) {
      return;
    }
    if (index > size) {
      return;
    }
    if (prev.length && !prev[prev.length - 1].includes("p") 
    && prev[prev.length - 1].length<3) {
      return;
    }
    for (let i = index; i < size; i++) {
      let newIndex = i + 1;
      var slice = A.slice(index, newIndex);
      prev.push(slice);
      dfs(k, newIndex, prev);
      prev.pop();
    }
  };
  for (let j = peakCount; j >= 1; j--) {
    if (peakCount % j != 0) {
      continue;
    }
    dfs(j, 0, []);
    if (found) {
      found = false;
      //console.log(`${j} is good`);
      return j;
    }
  }
  return 0;
}

var arr = [1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2];
console.log(solution(arr));
