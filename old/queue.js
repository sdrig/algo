 function Queue() {
  this._oldestIndex = 1;
  this._newestIndex = 1;
  this._storage = {};
}

Queue.prototype.size = function () {
  return this._newestIndex - this._oldestIndex;
};

Queue.prototype.enqueue = function (data) {
  this._storage[this._newestIndex] = data;
  this._newestIndex++;
};

Queue.prototype.dequeue = function () {
  var oldestIndex = this._oldestIndex,
    newestIndex = this._newestIndex,
    deletedData;

  if (oldestIndex !== newestIndex) {
    deletedData = this._storage[oldestIndex];
    delete this._storage[oldestIndex];
    this._oldestIndex++;

    return deletedData;
  }
};
Queue.prototype.storage = function () {
  return this._storage;
};

module.exports = Queue;
var q = new Queue();
const addQ = (n, q) => {
  q.enqueue(n);
  console.log(q.storage());
};
const removeQ= ( q) => {
    q.dequeue();
    console.log(q.storage());
  };
addQ(1, q);
addQ('asdasd', q);
addQ('asdasd', q);
addQ(4, q);
addQ(5, q);

removeQ(q);
removeQ(q);

addQ('yeni', q);

addQ('son', q);


removeQ(q);
removeQ(q);
removeQ(q);
removeQ(q);
removeQ(q);
removeQ(q);
