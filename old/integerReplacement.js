/**
 * @param {number} n
 * @return {number}
 */
var integerReplacement = function (n) {
  var minCount = Infinity;
  var go = (t, index, value, count) => {
    if (t <= 1) {
      minCount = Math.min(count, minCount);
      return;
    }
    var d = [1, -1];
    let op = 1;
    for (let i = 0; i < d.length; i++) {
      if (t % 2 == 0) {
        t = t / 2;
        op = 1;
      } else {
        t = t + d[i];
        op = 2;
      }
      count++;
      go(t, i + 1, d[i], count);
      console.log("change");
      if (op == 1) {
        t = t * 2;
      } else {
        t = t - d[i];
      }
    }
  };
  go(n, 0, 1, 0);
};

integerReplacement(10000);
