function solution(A) {
  // write your code in JavaScript (Node.js 8.9.4)
  var missing = 1;
  A.sort();
  for (var i = 0; i < A.length; i++) {
    if (A[i] == missing) {
      missing++;
    }
    if (A[i] > missing) {
      break;
    }
  }
  return missing;
}

solution([1, 1, 1, 1, 1]);
