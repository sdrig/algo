class MyGraph {
  constructor() {
    this.nodes = new Map();
  }
  addNode(label) {
    this.nodes.set(label, []);
  }
  addEdge(source, dest) {
    var adjencyListForSource = this.nodes.get(source);
    var adjencyListForDestination = this.nodes.get(dest);
    if (
      adjencyListForSource == undefined ||
      adjencyListForDestination == undefined
    ) {
      throw "Nodes could not found";
    }
    adjencyListForSource.push(dest);
    adjencyListForDestination.push(source);
  }
  removeNode(label) {
    var adjencyListForNode = this.nodes.get(label);
    for (var adjency of adjencyListForNode) {
    }
  }
  removeAdjency(adjency, adjencyListForNode) {
    var index = adjencyListForNode.indexOf(adjency);
    if (index != -1) {
      adjencyListForNode.splice(index, 1);
    }
  }
  display() {
    this.nodes.forEach((adjencyList, label) => {
      console.log(`${label}=>${adjencyList}`);
    });
  }
}

let g = new MyGraph();
g.addNode("a");
g.addNode("b");
g.addNode("c");
g.addNode("d");
g.addNode("e");

g.addEdge("a", "b");
g.addEdge("a", "c");
g.addEdge("b", "c");

g.display();
console.log;
