function solution(A) {
    let result = 0
    const frequence = {}
    const half = Math.floor(A.length / 2)
 
    let leaderFrequence = 0
    let leaderValue = 0
 
    for (let i = 0; i < A.length; i++) {
        if (frequence[A[i]] === undefined) {
            frequence[A[i]] = [i]
        } else {
            frequence[A[i]].push(i)
        }
        if (frequence[A[i]].length > half && frequence[A[i]].length > leaderFrequence) {
            leaderFrequence = frequence[A[i]].length
            leaderValue = A[i]
        }
    }
 
    let numOfLeaderInLeft = 0
 
    // return frequence[f][0]
    for (let j = 0; j < A.length; j++) {
 
        //length of left array
        const leftLen = j + 1
 
        //num of leaders in the left array
        if (A[j] === leaderValue) numOfLeaderInLeft++
 
        //length of right array
        const rightLen = A.length - leftLen
 
        // num of leaders in the right array
        const numOfLeaderInRight = leaderFrequence - numOfLeaderInLeft
 
        if (numOfLeaderInLeft > Math.floor(leftLen / 2) && numOfLeaderInRight > Math.floor(rightLen / 2)) {
            result++
        }
    }
 
    return result
}
var nums = [4, 3, 4, 4, 4, 2];
solution(nums);
