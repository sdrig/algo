function solution(A) {
  // write your code in JavaScript (Node.js 4.0.0)

  if (A.length < 3) {
    return 0;
  }

  var i = 0;
  var j = 0;
  var peaks = [];
  var maxSize = 0;

  for (i = 1; i < A.length - 1; i++) {
    if (A[i] > A[i - 1] && A[i] > A[i + 1]) {
      peaks.push(i);
    }
  }

  if (peaks.length < 2) {
    return peaks.length;
  }

  var limit = parseInt(Math.sqrt(A.length));

  for (i = 1; i <= A.length; i++) {
    if (A.length % i !== 0) {
      continue;
    }

    var blockSize = i;
    var blockCount = A.length / i;

    if (blockSize < 3) {
      continue;
    }

    var lastGroup = -1;
    for (j = 0; j < peaks.length; j++) {
      if (parseInt(peaks[j] / blockSize) === lastGroup + 1) {
        lastGroup++;
      }
    }

    if (lastGroup + 1 === blockCount) {
      if (blockCount > maxSize) {
        maxSize = blockCount;
      }
    }
  }

  return maxSize;
}

var arr = [1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2];
console.log(solution(arr));
