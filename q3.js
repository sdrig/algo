// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(S) {
    // write your code in JavaScript (Node.js 8.9.4)
    var strArray = S.split('')
    var aCount = strArray.filter(x => x == 'a').length;
    var size = S.length;
    if (aCount % 3 != 0) {
        return 0;
    }
    let ans = 0;
    const go = (arr, index) => {
        if (arr.length == 3 && index == size) {
            ans++;
            return ans;
        }
        var lastItem = arr.length > 0 ? arr[arr.length - 1] : null;
        if (aCount > 0 && lastItem && lastItem.indexOf('a') == -1) {
            return;
        }
        for (let i = index; i < size; i++) {
            const newIndex = i + 1;
            const slice = S.slice(index, newIndex);
            arr.push(slice);
            go(arr, newIndex);
            arr.pop();
        }
    }
    go([], 0);
    return ans;
}

s = 'babaa'

var r = solution(s);

console.log(r);



console.log(solution("aaaa")); // 0
console.log(solution("babaa")); // 2
console.log(solution("ababa")); // 4
console.log(solution("aba")); // 0
console.log(solution("bbbbb")); // 6

console.log(solution("babaa")); // 2
console.log(solution("ababa")); // 4
console.log(solution("aba")); // 0
console.log(solution("bbbbbbbbbbb")); // 6
