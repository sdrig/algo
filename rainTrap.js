/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function (nums) {
  const size = nums.length;
  var firstTrapIndex = nums.findIndex((x) => x > 0);
  var max1 = nums[firstTrapIndex];
  var min = [[firstTrapIndex, max1]];
  var q = [[firstTrapIndex, max1]];
  let result = 0;
  const countWater = (q, min) => {
    var temp = Array.from(nums);
    var firstTrapIndex = q[0][0];
    var lastTrapIndex = q[1][0];
    const minWaterIndex = min[0][0] - firstTrapIndex;
    var baseWater = min[0][1];
    var leftPtr = minWaterIndex - 1,
      rightPtr = minWaterIndex + 1;
    var slice = temp.slice(firstTrapIndex, lastTrapIndex + 1);
    lastTrapIndex = lastTrapIndex - firstTrapIndex;
    firstTrapIndex = 0;
    let ans = 0;
    while (0 <= leftPtr && rightPtr <= lastTrapIndex) {
      var firstHeight = slice[leftPtr];
      var lastHeight = slice[rightPtr];
      var minH = Math.min(firstHeight, lastHeight);
      var waterCount = (rightPtr - leftPtr - 1) * minH;
      slice = slice.map((curr, index) =>
        curr == baseWater ? curr : curr - waterCount
      );
      leftPtr--;
      rightPtr++;
      ans += waterCount;
    }
    return ans;
  };

  for (let i = firstTrapIndex; i < size; i++) {
    if (
      (q.length == 1 && nums[i + 1] >= q[0][1]) ||
      (nums[i + 1] >= min[0][1])
    ) {
      max1 = nums[i + 1];
      q.push([i + 1, max1]);
      console.log(`new max ${i} i:${nums[i]} i+1:${nums[i + 1]}`);
    } else {
      console.log(`dustu ${i} i:${nums[i]} i+1:${nums[i + 1]}`);
      if (!min.length || (nums[i + 1] <= min[0][1] && i + 1 != nums.length)) {
        min.length = 0;
        min.push([i + 1, nums[i + 1]]);
      }
    }
    if (q.length == 2) {
      console.log("iki tepe arasinda sikisan suyu bul. ve ilk tepeyi sil");
      result += countWater(q, min);
      q.shift();
      max1 = q[0][1];
      min = [[i + 1, nums[i + 1]]];
    }
  }
  return result;
};
var arr = [
  [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1],
  [2, 1, 0, 2, 1, 1, 1, 3, 2, 1, 2, 1],
  [2, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1],
  [3, 1, 1, 4, 1, 0, 0, 5, 4, 3, 3, 3],
];

for (var i = 0; i < arr.length; i++) {
  console.log(trap(arr[i]));
}
