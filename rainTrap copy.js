/**
 * @param {number[]} height
 * @return {number}
 */

var trap = function (nums) {
  var count = 0;
  let ans = 0;
  while (true) {
    count = nums.reduce((acc, curr) => (acc += curr > 1 ? 1 : 0), 0);
    var arr = nums.map((x) => (x - 1 > 0 ? 1 : x));
    var firstTrapIndex = arr.findIndex((x) => x == 1);
    var lastTrapIndex = arr.lastIndexOf(1);
    // ans += arr.reduce(
    //   (acc, curr, idx) =>
    //     (acc +=
    //       curr == 0 && idx >= firstTrapIndex && idx <= lastTrapIndex ? 1 : 0),
    //   0
    // );
    var firsTrapFounded = false;
    var tempCount = 0;
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] == 1) {
        firsTrapFounded = true;
      }
      if (arr[i] == 0 && firsTrapFounded) {
        tempCount += 1;
      }
      if (arr[i] == 1) {
        ans += tempCount;
        tempCount = 0;
      }
    }
    nums = nums.map((x) => (x != 0 ? x - 1 : x));
    if (count == 0) {
      break;
    }
  }
  return ans;
};

var arr = [
  [2, 0, 2],
  [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1],
  // [2, 1, 0, 2, 1, 1, 1, 3, 2, 1, 2, 1],
  // [2, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1],
  // [3, 1, 1, 4, 1, 0, 0, 5, 4, 3, 3, 3],
];

for (var i = 0; i < arr.length; i++) {
  console.log(trap(arr[i]));
}
