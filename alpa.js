// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(S) {
  // write your code in JavaScript (Node.js 8.9.4)
  const alphabet = "abcdefghijklmnopqrstuvwxyz";
  let map = new Map();
  let indexOfLarg = -Infinity;
  let i = 0;
  if (S.length <= 1) {
    return "NO";
  }
  const putMap = (w) => {
    var lower = w.toLowerCase();
    var upper = lower.toUpperCase();
    const keyLowerCase = `l-${lower}`;
    const keyUpperCase = `u-${upper}`;
    if (w == lower) {
      map.set(keyLowerCase, true);
    } else {
      map.set(keyUpperCase, true);
    }
    return {
      keyLowerCase: keyLowerCase,
      keyUpperCase: keyUpperCase,
      lower: lower,
    };
  };
  while (i < S.length) {
    var keys = putMap(S[i]);
    if (map.has(keys.keyLowerCase) && map.has(keys.keyUpperCase)) {
      indexOfLarg = Math.max(indexOfLarg, alphabet.indexOf(keys.lower));
    }
    i++;
  }
  return indexOfLarg != -Infinity ? alphabet[indexOfLarg].toUpperCase() : "NO";
}

var s = "aA";

solution(s);
