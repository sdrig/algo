// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(S, K) {
    // write your code in JavaScript (Node.js 8.9.4)

    if(S.length==1){
        return 0;
    }
    let min = Infinity;
    const compress = (arr)=>{
        let current = arr[0];
        let count = 1;
        let str = '';
        for(let i = 1;i<arr.length+1;i++){
            if(current!=arr[i]){
                str += (count == 1 ? '' : count ) + current;
                current = arr[i]
                count = 1;
            } else {
                count++
            }
        }
        console.log(str)
        return str;
    }
    for(let i = 0; i< S.length-K + 1;i++) {
        var temp = S.split('')
        var b = temp.splice(i,K)
        console.log(b,temp,i)
        min = Math.min(min,compress(temp).length)
    }
    return min;
}


s = 'AAAAAAAAAAABXXAAAAAAAAAA'
s = 'AAAAAAAAAAB'
s = 'ABCDAAA'
l = 3

var r= solution(s,l)

console.log(r);