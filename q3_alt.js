// you can write to stdout for debugging purposes, e.g.
// console.log('this is a debug message');

function solution(S) {
    // write your code in JavaScript (Node.js 8.9.4)
    var strArray = S.split('')
    var aCount = strArray.filter(x => x == 'a').length;
    var size = S.length;
    if (aCount % 3 != 0) {
        return 0;
    }
    const blockSize = aCount / 3;
    let indexOfBlocks = [];
    let ans = 0;
    let counter = 0;
    for (let i = 0; i < size; i++) {
        if (S[i] == 'a') {
            if (counter == 0 || (counter + 1) % blockSize == 0) {
                indexOfBlocks.push(i);
                counter= (counter + 1) % blockSize;
            }
        }
    }
}

s = 'babaa'
var r = solution(s);
console.log(r);



console.log(solution("aaaa")); // 0
console.log(solution("babaa")); // 2
console.log(solution("ababa")); // 4
console.log(solution("aba")); // 0
console.log(solution("bbbbb")); // 6

console.log(solution("babaa")); // 2
console.log(solution("ababa")); // 4
console.log(solution("aba")); // 0
console.log(solution("bbbbbbbbbbb")); // 6
